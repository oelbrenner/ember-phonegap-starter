# Reload the browser automatically whenever files change
activate :livereload

# Enable ember-middleman
activate :ember

# PhoneGap expects a 'www' directory to be present when you use the command line tools.
set :build_dir, 'www'

# Custom sprockets configuration used in the build process
set :test_dir,       'tests'
set :build_assets,   %w(application.js application.scss)

# Middleman configuration for sprockets
set :css_dir,        'stylesheets'
set :js_dir,         'javascripts'
set :images_dir,     'images'
set :fonts_dir,      'fonts'
set :bower_dir,       JSON.parse(File.read '.bowerrc')["directory"]

configure :build do
  # Minfiy the CSS.
  activate :minify_css

  # Minify the JS.
  activate :minify_javascript

  # Tells Middleman to handle asset URLs.
  activate :relative_assets

  # Ignore all tests on compilation
  ignore "#{settings.test_dir}/*"
  ignore 'tests.html.erb'

  # We want to ignore all assets that are not in the white list on compilation
  # as Sprockets will handle the compilation step. We will walk through the asset
  # tree looking for white listed files. This isn't recursive yet, so it only looks
  # for manifest type files in the root directories.

  %w(stylesheets javascripts).each do |asset_type|
    asset_dir = File.join(settings.source, asset_type)

    Dir.entries(asset_dir).each do |entry|
      next if %w(. ..).concat(settings.build_assets).include?(entry)

      middleman_path = File.join(asset_type, entry)

      if File.directory? File.join(asset_dir, entry)
        ignore(File.join middleman_path, '*')
      else
        ignore(middleman_path)
      end
    end
  end
end

# Configuration to tell sprockets to compile things inside of /tests
configure :development do
  test_path = settings.test_dir
  our_sprockets = sprockets
  our_sprockets.append_path test_path
  map("/#{test_path}") { run our_sprockets }
end

# Use bower compontents in the asset pipeline.
ready do
  sprockets.append_path File.join('..', settings.bower_dir)
end
