//= require test_helper
//= require_self
//= require_tree ./unit
//= require_tree ./integration

/* global emq, setResolver */

// Ember configuration
App.rootElement = '#ember-testing';
App.setupForTesting();
App.injectTestHelpers();

// QUnit configuration
emq.globalize();
setResolver(Ember.DefaultResolver.create({namespace: App}));
