Hybrid app development kit with Ember and PhoneGap
==================================================

This kit will get you started building a PhoneGap application using Middleman.


Getting Started with Development
--------------------------------

    rake setup
    rake server
    open http://localhost:4567/


Compiling
---------

There are various rake tasks for building and compiling the app. To run in the iOS simulator simply use:

    rake emulate:ios

And to install on an Android device:

    rake install:android[DEVICE_ID]



Whats included?
---------------

Since it is using Middleman any of the asset pipeline gems for Ruby on Rails *should* work. YMMV.

 * jQuery 2.x
 * Ember.js
 * Handlebars
 * Ratchet



Tests
-----

This starter kit comes with an integration test sample, written for QUnit runner.

You can run the tests by opening the `http://localhost:4567/tests.html` page in your browser.

The test is located in the `source/tests/integration/sanity_test.js` file. You can see how such an
integration test should be written, using QUnit assertions and ember-testing helpers.

For more information about ember-testing package see [ember-testing](http://emberjs.com/guides/testing/integration/)

For more information about the QUnit testing framework, see [QUnit](http://qunitjs.com/)
